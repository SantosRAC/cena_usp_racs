#!/usr/bin/python

import yaml

configfile: "config.yaml"
hmmpress = config["hmmpress"]

rule all:
    input:
        "dbCAN-HMMdb-V8.txt.h3p"

rule download_dbcan_hmms:
    output:
        dbcan_hmms = "dbCAN-HMMdb-V8.txt",
        hmmparser = "hmmscan-parser.gz"
    shell:
        """
        wget https://bcb.unl.edu/dbCAN2/download/Databases/{output.dbcan_hmms}
        wget https://bcb.unl.edu/dbCAN2/download/Tools/{output.hmmparser}
        """

rule hmmpress_dbcan:
    input:
        dbcan_hmms = "dbCAN-HMMdb-V8.txt"
    output:
        "dbCAN-HMMdb-V8.txt.h3p"
    shell:
        """
        hmmpress {input.dbcan_hmms}
        """
