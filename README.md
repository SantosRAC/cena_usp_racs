# CENA_USP_RACS



## Content

 * Database with RNAseq metadata for the projects involving CoNekT-Bioenergy;
 * Scripts to manage downloaded SRA data, store/query Sqlite3 databases, and general purpose activities of my (RACS, [@SantosRAC](https://gitlab.com/SantosRAC) ) post-doc research with Prof. Dr. rer. nat Diego M. Riaño Pachón
