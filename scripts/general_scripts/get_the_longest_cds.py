#!/usr/bin/env python

import argparse
from Bio import SeqIO
from io import StringIO

version = 0.02
parser = argparse.ArgumentParser(description='Get the longest CDS from orthogroups.',
                                 add_help=True)
parser.add_argument('-v', '--version', action='version', version=version)
parser.add_argument('--fasta', dest='cds_file', metavar='all_cds.fasta',
                    type=str, help='File with all CDSs of interest.',
                    required=True)
parser.add_argument('--orthogroups', dest='orthogroup_file',
                    metavar='sequences_classification.tsv',
                    type=str, help='TSV with a. classification, b. orthogroup, and c. sequence ID.',
                    required=True)

args = parser.parse_args()
cds_f = args.cds_file
orthogroups_f = args.orthogroup_file

cds_to_groups = {}

with open(orthogroups_f, 'r') as of:
    for line in of:
        cds_class, orthogroup, cds_id = line.strip().split('\t')
        if orthogroup in cds_to_groups.keys():
            if cds_id in cds_to_groups[orthogroup].keys():
                if (cds_class in ['Soft-core', 'Hard-core']) and \
                    (cds_to_groups[orthogroup][cds_id] in ['Soft-core', 'Hard-core']):
                    #print(f'This conflict for {cds_id} is allowed ({cds_class} vs. {cds_to_groups[orthogroup][cds_id]})')
                    pass
                else:
                    #print(f'This conflict is NOT allowed ({cds_class} vs. {cds_to_groups[orthogroup][cds_id]})')
                    exit(1)
            cds_to_groups[orthogroup][cds_id] = cds_class
        else:
            cds_to_groups[orthogroup] = {cds_id: cds_class}

gb_vrl = SeqIO.index_db(cds_f+".idx", cds_f, "fasta")

for orthogroup_id in cds_to_groups.keys():
    longest_cds_in_og = ''
    long_cds_seq = ''
    long_cds_len = 0
    for sequence_id in list(cds_to_groups[orthogroup_id]):
        #print(sequence_id)
        #TODO: add argparse option for changing this behavior currently done for TUC717
        if not sequence_id.startswith('TUC'):
            fasta_io = StringIO(gb_vrl.get_raw(sequence_id).decode("utf-8"))
            records = SeqIO.parse(fasta_io, "fasta")
            for rec in records:
                #print(rec.id, len(rec.seq))
                if len(rec.seq) > long_cds_len:
                    long_cds_len = len(rec.seq)
                    long_cds_seq = rec.seq
                    longest_cds_in_og = rec.id
            fasta_io.close()
    if longest_cds_in_og and (len(long_cds_seq) != 0):
        print(f'>{longest_cds_in_og} ({orthogroup_id})\n{long_cds_seq}')
    