#!/usr/bin/env python

from Bio import Entrez
import time

# TODO: add argparse
# TODO: connect to sqlite3 (e.g., to check if literature already exists)
email_address = "renatoacsantos@gmail.com"
pmid = 25533953

Entrez.email = email_address

handle = Entrez.elink(dbfrom="pubmed", id=pmid, linkname="pubmed_pubmed")
record = Entrez.read(handle)
handle.close()

for assoc_pmid in record[0]['LinkSetDb'][0]['Link']:
    # TODO: implement filters (e.g., by date or if literature do not exist in our db)
    time.sleep(1)
    print(assoc_pmid['Id'])
    handle = Entrez.esummary(db="pubmed", id=assoc_pmid['Id'], retmode="text")
    record = Entrez.read(handle)
    handle.close()
    print(record[0]['Title'])

