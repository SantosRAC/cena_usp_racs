#!/home/santosrac/anaconda3/bin/python3
"""Script that parses annotation as dowloaded from SolGenomics to\
     recover specific information."""

import argparse
import pandas as pd
import os

version=0.01
parser = argparse.ArgumentParser(description='Reads a tabular file with annotation from SolGenomics and recover specific gene information', add_help=True)
parser.add_argument('-v','--version', action='version', version=version)
parser.add_argument('-i','--in', dest='inANNOT', metavar='in_table.txt', type=str, help='Input table with all annotation in SolGenomics.', required=True)
parser.add_argument('-o','--out', dest='outANNOT', metavar='out_table.txt', type=str, help='Output table with specific annotation.', required=True)

#
args = parser.parse_args()
inputAnnotOBJ = args.inANNOT
inputannotation_df = pd.read_table(inputAnnotOBJ)
outputAnnotOBJ = args.outANNOT
tableOUT = open(outputAnnotOBJ, "w")

#
filtered_df = (inputannotation_df.loc[inputannotation_df.GO.notnull()])[["gene_name","GO"]]
filtered_df.to_csv('tmp.txt', sep='\t', index=False, header=False)

#
go_dict = {}
go_file = open('tmp.txt')

# 
tableOUT.write("gene_name\tGO\tevidence_tag\n")
for line in go_file:
    line = line.strip()
    [gene_name, go_info] = line.split('\t')
    go_info_list = go_info.split(',')
    for go in go_info_list:
        go = go.replace(' ', '')
        # Adding ISM (Inferred from Sequence Model), assuming InterProScan was used
        if go.startswith('GO:'):
            tableOUT.write(gene_name+"\t"+go+"\tISM\n")

tableOUT.close()

# Delete temporary file
try:
    os.remove('tmp.txt')
except OSError as e:  ## if failed, report it back to the user ##
    print ("Error: %s - %s." % (e.filename, e.strerror))