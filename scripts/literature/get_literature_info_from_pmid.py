#!/usr/bin/env python

import argparse
import sqlite3
from Bio import Entrez
import time

version = 0.01
parser = argparse.ArgumentParser(description='Recovers literature information\
                                 from sqlite database.', add_help=True)
parser.add_argument('-v', '--version', action='version', version=version)
parser.add_argument('--email', dest='e_mail',
                    metavar='youemail@somewherehappytolive.com', type=str,
                    help='E-mail address for Entrez',
                    required=True)
parser.add_argument('--database', dest='db',
                    metavar='literature.db', type=str,
                    help='Database name',
                    required=True)
parser.add_argument('--output', dest='outtable',
                    metavar='literature.tsv', type=str,
                    help='Output table name',
                    required=True)

args = parser.parse_args()
email_address = args.e_mail
out_table = args.outtable

conn = sqlite3.connect(args.db)
c = conn.cursor()
c.execute('''SELECT DISTINCT pmid FROM sra_metadata WHERE pmid!=""''')
pmids = c.fetchall()
conn.commit()
conn.close()

Entrez.email = email_address

outtable_obj = open(out_table, 'a')
outtable_obj.write('pmid\ttitle\tPubDate\tDOI\tLastAuthor\tSource\n')
outtable_obj.close()

for pmid in pmids:
    handle = Entrez.esummary(db="pubmed", id=pmid[0], retmode="text")
    record_pmid = Entrez.read(handle)
    handle.close()
    if 'DOI' in record_pmid[0].keys():
        outtable_obj = open(out_table, 'a')
        outtable_obj.write(f'{pmid[0]}\t{record_pmid[0]["Title"]}\t{record_pmid[0]["PubDate"].split(" ")[0]}\t \
              {record_pmid[0]["DOI"]}\t{record_pmid[0]["LastAuthor"]}\t{record_pmid[0]["Source"]}\n')
        outtable_obj.close()
    time.sleep(1)