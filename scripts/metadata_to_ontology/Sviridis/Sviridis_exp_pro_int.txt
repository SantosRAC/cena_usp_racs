SampleID	ConditionDescription	Strandness	Layout	PO_column	PECO_column
SRR17521799	inflorescence	strand specific	paired-end	PO:0009049
SRR17521800	inflorescence	strand specific	paired-end	PO:0009049
SRR17521801	inflorescence	strand specific	paired-end	PO:0009049
SRR17521802	inflorescence	strand specific	paired-end	PO:0009049
SRR17521803	inflorescence	strand specific	paired-end	PO:0009049
SRR17521804	inflorescence	strand specific	paired-end	PO:0009049
SRR17521805	inflorescence	strand specific	paired-end	PO:0009049
SRR17521806	inflorescence	strand specific	paired-end	PO:0009049
SRR17521807	inflorescence	strand specific	paired-end	PO:0009049
SRR17521808	inflorescence	strand specific	paired-end	PO:0009049
SRR17521809	inflorescence	strand specific	paired-end	PO:0009049
SRR17521810	inflorescence	strand specific	paired-end	PO:0009049
SRR17521811	inflorescence	strand specific	paired-end	PO:0009049
SRR17521812	inflorescence	strand specific	paired-end	PO:0009049
SRR17521813	inflorescence	strand specific	paired-end	PO:0009049
SRR17521814	inflorescence	strand specific	paired-end	PO:0009049
SRR17521815	inflorescence	strand specific	paired-end	PO:0009049
SRR17521816	inflorescence	strand specific	paired-end	PO:0009049
SRR17521817	inflorescence	strand specific	paired-end	PO:0009049
SRR17521818	inflorescence	strand specific	paired-end	PO:0009049
SRR17521819	inflorescence	strand specific	paired-end	PO:0009049
SRR17521820	inflorescence	strand specific	paired-end	PO:0009049
SRR16937473	panicle inflorescence	strand specific	paired-end	PO:0030123
SRR16937474	panicle inflorescence	strand specific	paired-end	PO:0030123
SRR126937475	panicle inflorescence	strand specific	paired-end	PO:0030123
SRR16937476	panicle inflorescence	strand specific	paired-end	PO:0030123
SRR16937477	panicle inflorescence	strand specific	paired-end	PO:0030123
SRR16937478	panicle inflorescence	strand specific	paired-end	PO:0030123
SRR14828134	leaf	strand specific	paired-end	PO:0025034
SRR14828133	leaf	strand specific	paired-end	PO:0025034
SRR14828132	leaf	strand specific	paired-end	PO:0025034
SRR14828131	leaf	strand specific	paired-end	PO:0025034
SRR14828122	leaf	strand specific	paired-end	PO:0025034
SRR14828121	leaf	strand specific	paired-end	PO:0025034
SRR14828120	leaf	strand specific	paired-end	PO:0025034
SRR14828119	leaf	strand specific	paired-end	PO:0025034
SRR14828110	leaf	strand specific	paired-end	PO:0025034
SRR14828109	leaf	strand specific	paired-end	PO:0025034
SRR14828108	leaf	strand specific	paired-end	PO:0025034
SRR14828107	leaf	strand specific	paired-end	PO:0025034
SRR14828130	leaf	strand specific	paired-end	PO:0025034
SRR14828129	leaf	strand specific	paired-end	PO:0025034
SRR14828128	leaf	strand specific	paired-end	PO:0025034
SRR14828127	leaf	strand specific	paired-end	PO:0025034
SRR14828118	leaf	strand specific	paired-end	PO:0025034
SRR14828117	leaf	strand specific	paired-end	PO:0025034
SRR14828116	leaf	strand specific	paired-end	PO:0025034
SRR14828115	leaf	strand specific	paired-end	PO:0025034
SRR14828106	leaf	strand specific	paired-end	PO:0025034
SRR14828105	leaf	strand specific	paired-end	PO:0025034
SRR14828104	leaf	strand specific	paired-end	PO:0025034
SRR14828103	leaf	strand specific	paired-end	PO:0025034
SRR14828126	leaf	strand specific	paired-end	PO:0025034
SRR14828125	leaf	strand specific	paired-end	PO:0025034
SRR14828124	leaf	strand specific	paired-end	PO:0025034
SRR14828123	leaf	strand specific	paired-end	PO:0025034
SRR14828114	leaf	strand specific	paired-end	PO:0025034
SRR14828113	leaf	strand specific	paired-end	PO:0025034
SRR14828112	leaf	strand specific	paired-end	PO:0025034
SRR14828111	leaf	strand specific	paired-end	PO:0025034
SRR14828102	leaf	strand specific	paired-end	PO:0025034
SRR14828101	leaf	strand specific	paired-end	PO:0025034
SRR14828100	leaf	strand specific	paired-end	PO:0025034
SRR14828099	leaf	strand specific	paired-end	PO:0025034
SRR14828098	leaf	strand specific	paired-end	PO:0025034
SRR14828097	leaf	strand specific	paired-end	PO:0025034
SRR14828096	leaf	strand specific	paired-end	PO:0025034
SRR14828095	leaf	strand specific	paired-end	PO:0025034
SRR12763004	leaf	strand specific	paired-end	PO:0025034
SRR12763005	leaf	strand specific	paired-end	PO:0025034
SRR11794958	leaf	strand specific	paired-end	PO:0025034
SRR11794959	leaf	strand specific	paired-end	PO:0025034
SRR11794960	leaf	strand specific	paired-end	PO:0025034
SRR11794964	leaf	strand specific	paired-end	PO:0025034
SRR11794965	leaf	strand specific	paired-end	PO:0025034
SRR11794966	leaf	strand specific	paired-end	PO:0025034
SRR11794961	leaf	strand specific	paired-end	PO:0025034
SRR11450746	root	strand specific	paired-end	PO:0009005
SRR11450747	root	strand specific	paired-end	PO:0009005
SRR11450754	root	strand specific	paired-end	PO:0009005
SRR11450755	root	strand specific	paired-end	PO:0009005
SRR7472267	root	strand specific	paired-end	PO:0009005
SRR7472268	root	strand specific	paired-end	PO:0009005
SRR7472276	root	strand specific	paired-end	PO:0009005
SRR5498169	root	strand specific	paired-end	PO:0009005
SRR5498158	root	strand specific	paired-end	PO:0009005
SRR5498139	root	strand specific	paired-end	PO:0009005
SRR11450748	shoot system	strand specific	paired-end	PO:0009006
SRR11450749	shoot system	strand specific	paired-end	PO:0009006
SRR11450750	shoot system	strand specific	paired-end	PO:0009006
SRR11450751	shoot system	strand specific	paired-end	PO:0009006
SRR11450752	leaf	strand specific	paired-end	PO:0025034
SRR11450753	leaf	strand specific	paired-end	PO:0025034
SRR11450756	leaf	strand specific	paired-end	PO:0025034
SRR11450757	leaf	strand specific	paired-end	PO:0025034
SRR7472535	leaf	strand specific	paired-end	PO:0025034
SRR7472540	leaf	strand specific	paired-end	PO:0025034
SRR7472541	leaf	strand specific	paired-end	PO:0025034
SRR7472543	leaf	strand specific	paired-end	PO:0025034
SRR7472544	leaf	strand specific	paired-end	PO:0025034
SRR7472545	leaf	strand specific	paired-end	PO:0025034
SRR7472546	leaf	strand specific	paired-end	PO:0025034
SRR7472547	leaf	strand specific	paired-end	PO:0025034
SRR7472549	leaf	strand specific	paired-end	PO:0025034
SRR7472551	leaf	strand specific	paired-end	PO:0025034
SRR7472554	leaf	strand specific	paired-end	PO:0025034
SRR7472556	leaf	strand specific	paired-end	PO:0025034
SRR7472557	leaf	strand specific	paired-end	PO:0025034
SRR7472563	leaf	strand specific	paired-end	PO:0025034
SRR7472567	leaf	strand specific	paired-end	PO:0025034
SRR7472570	leaf	strand specific	paired-end	PO:0025034
SRR7472572	leaf	strand specific	paired-end	PO:0025034
SRR7472573	leaf	strand specific	paired-end	PO:0025034
SRR7472265	leaf	strand specific	paired-end	PO:0025034
SRR7472266	leaf	strand specific	paired-end	PO:0025034
SRR7472269	leaf	strand specific	paired-end	PO:0025034
SRR7472271	leaf	strand specific	paired-end	PO:0025034
SRR7472272	leaf	strand specific	paired-end	PO:0025034
SRR7472273	leaf	strand specific	paired-end	PO:0025034
SRR7472274	leaf	strand specific	paired-end	PO:0025034
SRR7472275	leaf	strand specific	paired-end	PO:0025034
SRR7472277	leaf	strand specific	paired-end	PO:0025034
SRR5578732	leaf	strand specific	paired-end	PO:0025034
SRR5578733	leaf	strand specific	paired-end	PO:0025034
SRR5578734	leaf	strand specific	paired-end	PO:0025034
SRR5498183	leaf	strand specific	paired-end	PO:0025034
SRR5498182	leaf	strand specific	paired-end	PO:0025034
SRR5498181	leaf	strand specific	paired-end	PO:0025034
SRR5498176	leaf	strand specific	paired-end	PO:0025034
SRR5498173	leaf	strand specific	paired-end	PO:0025034
SRR5498168	leaf	strand specific	paired-end	PO:0025034
SRR5498163	leaf	strand specific	paired-end	PO:0025034
SRR5498157	leaf	strand specific	paired-end	PO:0025034
SRR5498154	leaf	strand specific	paired-end	PO:0025034
SRR5498149	leaf	strand specific	paired-end	PO:0025034
SRR5498148	leaf	strand specific	paired-end	PO:0025034
SRR5498147	leaf	strand specific	paired-end	PO:0025034
SRR5498144	leaf	strand specific	paired-end	PO:0025034
SRR5498143	leaf	strand specific	paired-end	PO:0025034
SRR5498142	leaf	strand specific	paired-end	PO:0025034
SRR5498138	leaf	strand specific	paired-end	PO:0025034
SRR11280745	leaf	strand specific	paired-end	PO:0025034
SRR11280744	leaf	strand specific	paired-end	PO:0025034
SRR11280743	leaf	strand specific	paired-end	PO:0025034
SRR11280742	leaf	strand specific	paired-end	PO:0025034
SRR11280741	leaf	strand specific	paired-end	PO:0025034
SRR11280740	leaf	strand specific	paired-end	PO:0025034
SRR10489077	leaf	strand specific	paired-end	PO:0025034
SRR10489078	leaf	strand specific	paired-end	PO:0025034
SRR10489079	leaf	strand specific	paired-end	PO:0025034
SRR10489080	leaf	strand specific	paired-end	PO:0025034
SRR10489081	leaf	strand specific	paired-end	PO:0025034
SRR10489082	leaf	strand specific	paired-end	PO:0025034
SRR10489083	leaf	strand specific	paired-end	PO:0025034
SRR10489084	leaf	strand specific	paired-end	PO:0025034
SRR10489085	leaf	strand specific	paired-end	PO:0025034
SRR10489086	leaf	strand specific	paired-end	PO:0025034
SRR10489087	leaf	strand specific	paired-end	PO:0025034
SRR10489088	leaf	strand specific	paired-end	PO:0025034
SRR10489089	leaf	strand specific	paired-end	PO:0025034
SRR10489090	leaf	strand specific	paired-end	PO:0025034
SRR10489091	leaf	strand specific	paired-end	PO:0025034
SRR10489092	leaf	strand specific	paired-end	PO:0025034
SRR10489093	leaf	strand specific	paired-end	PO:0025034
SRR10489094	leaf	strand specific	paired-end	PO:0025034
SRR10489095	leaf	strand specific	paired-end	PO:0025034
SRR10489096	leaf	strand specific	paired-end	PO:0025034
SRR10489097	leaf	strand specific	paired-end	PO:0025034
SRR10489098	leaf	strand specific	paired-end	PO:0025034
SRR10489099	leaf	strand specific	paired-end	PO:0025034
SRR10489100	leaf	strand specific	paired-end	PO:0025034
SRR10489101	leaf	strand specific	paired-end	PO:0025034
SRR10489102	leaf	strand specific	paired-end	PO:0025034
SRR10489103	leaf	strand specific	paired-end	PO:0025034
SRR10489104	leaf	strand specific	paired-end	PO:0025034
SRR10489105	leaf	strand specific	paired-end	PO:0025034
SRR10489106	leaf	strand specific	paired-end	PO:0025034
SRR10489107	leaf	strand specific	paired-end	PO:0025034
SRR10489108	leaf	strand specific	paired-end	PO:0025034
SRR10489109	leaf	strand specific	paired-end	PO:0025034
SRR10489110	leaf	strand specific	paired-end	PO:0025034
SRR10489111	leaf	strand specific	paired-end	PO:0025034
SRR10489112	leaf	strand specific	paired-end	PO:0025034
SRR9099092	seed	strand specific	paired-end	PO:0009010
SRR9099091	seed	strand specific	paired-end	PO:0009010
SRR9099090	seed	strand specific	paired-end	PO:0009010
SRR9099089	seed	strand specific	paired-end	PO:0009010
SRR9099088	seed	strand specific	paired-end	PO:0009010
SRR9099087	seed	strand specific	paired-end	PO:0009010
SRR9099086	seed	strand specific	paired-end	PO:0009010
SRR9099085	seed	strand specific	paired-end	PO:0009010
SRR9099076	seed	strand specific	paired-end	PO:0009010
SRR9099075	seed	strand specific	paired-end	PO:0009010
SRR9099074	seed	strand specific	paired-end	PO:0009010
SRR9099073	seed	strand specific	paired-end	PO:0009010
SRR9099072	seed	strand specific	paired-end	PO:0009010
SRR9099071	seed	strand specific	paired-end	PO:0009010
SRR9099070	seed	strand specific	paired-end	PO:0009010
SRR9099069	seed	strand specific	paired-end	PO:0009010
SRR9099084	seed	strand specific	paired-end	PO:0009010
SRR9099083	seed	strand specific	paired-end	PO:0009010
SRR9099082	seed	strand specific	paired-end	PO:0009010
SRR9099081	seed	strand specific	paired-end	PO:0009010
SRR9099068	seed	strand specific	paired-end	PO:0009010
SRR9099067	seed	strand specific	paired-end	PO:0009010
SRR9099066	seed	strand specific	paired-end	PO:0009010
SRR9099065	seed	strand specific	paired-end	PO:0009010
SRR9099080	seed	strand specific	paired-end	PO:0009010
SRR9099079	seed	strand specific	paired-end	PO:0009010
SRR9099078	seed	strand specific	paired-end	PO:0009010
SRR9099077	seed	strand specific	paired-end	PO:0009010
SRR9099064	seed	strand specific	paired-end	PO:0009010
SRR9099063	seed	strand specific	paired-end	PO:0009010
SRR9099062	seed	strand specific	paired-end	PO:0009010
SRR9099061	seed	strand specific	paired-end	PO:0009010
SRR8742446	leaf	strand specific	paired-end	PO:0025034
SRR8742445	leaf	strand specific	paired-end	PO:0025034
SRR8742444	leaf	strand specific	paired-end	PO:0025034
SRR7704037	inflorescence	strand specific	paired-end	PO:0009049
SRR7704035	inflorescence	strand specific	paired-end	PO:0009049
SRR7704034	inflorescence	strand specific	paired-end	PO:0009049
SRR7704033	inflorescence	strand specific	paired-end	PO:0009049
SRR7704032	inflorescence	strand specific	paired-end	PO:0009049
SRR7704031	inflorescence	strand specific	paired-end	PO:0009049
SRR7704030	inflorescence	strand specific	paired-end	PO:0009049
SRR7704029	inflorescence	strand specific	paired-end	PO:0009049
SRR7704028	inflorescence	strand specific	paired-end	PO:0009049
SRR7704027	inflorescence	strand specific	paired-end	PO:0009049
SRR7704036	inflorescence	strand specific	paired-end	PO:0009049
ERR1942989	stem	strand specific	paired-end	PO:0009047
ERR1942987	stem	strand specific	paired-end	PO:0009047
ERR1942986	stem	strand specific	paired-end	PO:0009047
ERR1942985	stem	strand specific	paired-end	PO:0009047
ERR1942984	stem	strand specific	paired-end	PO:0009047
ERR1942983	stem	strand specific	paired-end	PO:0009047
ERR1942982	stem	strand specific	paired-end	PO:0009047
ERR1942981	stem	strand specific	paired-end	PO:0009047
ERR1942988	stem	strand specific	paired-end	PO:0009047
SRR5683480	root	strand specific	paired-end	PO:0009005
SRR5683481	root	strand specific	paired-end	PO:0009005
SRR5683482	root	strand specific	paired-end	PO:0009005
SRR5683483	root	strand specific	paired-end	PO:0009005
SRR5683484	root	strand specific	paired-end	PO:0009005
SRR5683485	root	strand specific	paired-end	PO:0009005
SRR5683486	root	strand specific	paired-end	PO:0009005
SRR5683487	root	strand specific	paired-end	PO:0009005
SRR5683488	root	strand specific	paired-end	PO:0009005
SRR5578728	panicle inflorescence	strand specific	paired-end	PO:0030123
SRR5498162	panicle inflorescence	strand specific	paired-end	PO:0030123
SRR5498161	panicle inflorescence	strand specific	paired-end	PO:0030123
SRR5498156	panicle inflorescence	strand specific	paired-end	PO:0030123
SRR5498178	shoot system	strand specific	paired-end	PO:0009006
SRR5498172	shoot system	strand specific	paired-end	PO:0009006
SRR5498167	shoot system	strand specific	paired-end	PO:0009006
SRR5445629	leaf	strand specific	paired-end	PO:0025034
SRR5445628	leaf	strand specific	paired-end	PO:0025034
SRR5445627	leaf	strand specific	paired-end	PO:0025034
SRR5445626	leaf	strand specific	paired-end	PO:0025034
SRR5445625	leaf	strand specific	paired-end	PO:0025034
SRR5445624	leaf	strand specific	paired-end	PO:0025034
SRR5445623	leaf	strand specific	paired-end	PO:0025034
SRR5445622	leaf	strand specific	paired-end	PO:0025034
SRR5445621	leaf	strand specific	paired-end	PO:0025034
SRR5445620	leaf	strand specific	paired-end	PO:0025034
SRR5445619	leaf	strand specific	paired-end	PO:0025034
SRR5445618	leaf	strand specific	paired-end	PO:0025034
SRR5445617	leaf	strand specific	paired-end	PO:0025034
SRR5445616	leaf	strand specific	paired-end	PO:0025034
SRR5445615	leaf	strand specific	paired-end	PO:0025034
SRR5445614	leaf	strand specific	paired-end	PO:0025034
SRR5445613	leaf	strand specific	paired-end	PO:0025034
SRR5445612	leaf	strand specific	paired-end	PO:0025034
SRR5445611	leaf	strand specific	paired-end	PO:0025034
SRR5445610	leaf	strand specific	paired-end	PO:0025034
SRR5445609	leaf	strand specific	paired-end	PO:0025034
SRR5445608	leaf	strand specific	paired-end	PO:0025034
SRR5445607	leaf	strand specific	paired-end	PO:0025034
SRR5445606	leaf	strand specific	paired-end	PO:0025034
SRR5445605	leaf	strand specific	paired-end	PO:0025034
SRR5445604	leaf	strand specific	paired-end	PO:0025034
SRR5445603	leaf	strand specific	paired-end	PO:0025034
SRR5445602	leaf	strand specific	paired-end	PO:0025034
SRR5445601	leaf	strand specific	paired-end	PO:0025034
SRR5445600	leaf	strand specific	paired-end	PO:0025034
SRR5445599	leaf	strand specific	paired-end	PO:0025034
SRR5445598	leaf	strand specific	paired-end	PO:0025034
SRR5445597	leaf	strand specific	paired-end	PO:0025034
SRR5445596	leaf	strand specific	paired-end	PO:0025034
SRR5445595	leaf	strand specific	paired-end	PO:0025034
SRR5445594	leaf	strand specific	paired-end	PO:0025034
SRR5445593	leaf	strand specific	paired-end	PO:0025034
SRR5445592	leaf	strand specific	paired-end	PO:0025034
SRR5445591	leaf	strand specific	paired-end	PO:0025034
SRR5445590	leaf	strand specific	paired-end	PO:0025034
SRR5445589	leaf	strand specific	paired-end	PO:0025034
SRR5445588	leaf	strand specific	paired-end	PO:0025034
SRR5445587	leaf	strand specific	paired-end	PO:0025034
SRR5445586	leaf	strand specific	paired-end	PO:0025034
SRR5445585	leaf	strand specific	paired-end	PO:0025034
SRR5445584	leaf	strand specific	paired-end	PO:0025034
SRR5445583	leaf	strand specific	paired-end	PO:0025034
SRR5445582	leaf	strand specific	paired-end	PO:0025034
SRR5445581	leaf	strand specific	paired-end	PO:0025034
SRR5445580	leaf	strand specific	paired-end	PO:0025034
SRR5445579	leaf	strand specific	paired-end	PO:0025034
SRR5445578	leaf	strand specific	paired-end	PO:0025034
SRR5445577	leaf	strand specific	paired-end	PO:0025034
SRR5445576	leaf	strand specific	paired-end	PO:0025034
SRR5445575	leaf	strand specific	paired-end	PO:0025034
SRR5445574	leaf	strand specific	paired-end	PO:0025034
SRR5445573	leaf	strand specific	paired-end	PO:0025034
SRR5445572	leaf	strand specific	paired-end	PO:0025034
SRR5445571	leaf	strand specific	paired-end	PO:0025034
SRR5445570	leaf	strand specific	paired-end	PO:0025034
SRR5445569	leaf	strand specific	paired-end	PO:0025034
SRR5445568	leaf	strand specific	paired-end	PO:0025034
SRR5445567	leaf	strand specific	paired-end	PO:0025034
SRR5445566	leaf	strand specific	paired-end	PO:0025034
SRR5445565	leaf	strand specific	paired-end	PO:0025034
SRR5445564	leaf	strand specific	paired-end	PO:0025034
SRR5445563	leaf	strand specific	paired-end	PO:0025034
SRR5445562	leaf	strand specific	paired-end	PO:0025034
SRR5445561	leaf	strand specific	paired-end	PO:0025034
SRR5445560	leaf	strand specific	paired-end	PO:0025034
SRR5445559	leaf	strand specific	paired-end	PO:0025034
SRR5445558	leaf	strand specific	paired-end	PO:0025034
SRR5445557	leaf	strand specific	paired-end	PO:0025034
SRR5445556	leaf	strand specific	paired-end	PO:0025034
SRR5445555	leaf	strand specific	paired-end	PO:0025034
SRR5445554	leaf	strand specific	paired-end	PO:0025034
SRR5445553	leaf	strand specific	paired-end	PO:0025034
SRR5445552	leaf	strand specific	paired-end	PO:0025034
SRR5445551	leaf	strand specific	paired-end	PO:0025034
SRR5445550	leaf	strand specific	paired-end	PO:0025034
SRR5445549	leaf	strand specific	paired-end	PO:0025034
SRR5445548	leaf	strand specific	paired-end	PO:0025034
SRR5445547	leaf	strand specific	paired-end	PO:0025034
SRR5445546	leaf	strand specific	paired-end	PO:0025034
SRR5445545	leaf	strand specific	paired-end	PO:0025034
SRR5445544	leaf	strand specific	paired-end	PO:0025034
SRR5445543	leaf	strand specific	paired-end	PO:0025034
SRR5445542	leaf	strand specific	paired-end	PO:0025034
SRR5445541	leaf	strand specific	paired-end	PO:0025034
SRR5445540	leaf	strand specific	paired-end	PO:0025034
SRR5445539	leaf	strand specific	paired-end	PO:0025034
SRR5445538	leaf	strand specific	paired-end	PO:0025034
SRR5445537	leaf	strand specific	paired-end	PO:0025034
SRR5445536	leaf	strand specific	paired-end	PO:0025034
SRR5445535	leaf	strand specific	paired-end	PO:0025034
SRR5445534	leaf	strand specific	paired-end	PO:0025034
SRR5021000	panicle inflorescence	strand specific	paired-end	PO:0030123
SRR5021001	panicle inflorescence	strand specific	paired-end	PO:0030123
SRR5021002	panicle inflorescence	strand specific	paired-end	PO:0030123
SRR5021003	panicle inflorescence	strand specific	paired-end	PO:0030123
SRR5020997	panicle inflorescence	strand specific	paired-end	PO:0030123
SRR5020998	panicle inflorescence	strand specific	paired-end	PO:0030123
SRR5020999	panicle inflorescence	strand specific	paired-end	PO:0030123
SRR5020994	shoot system	strand specific	paired-end	PO:0009006
SRR5020995	shoot system	strand specific	paired-end	PO:0009006
SRR5020996	shoot system	strand specific	paired-end	PO:0009006
SRR5020986	shoot system	strand specific	paired-end	PO:0009006
SRR5020987	shoot system	strand specific	paired-end	PO:0009006
SRR5020988	shoot system	strand specific	paired-end	PO:0009006
SRR5020989	shoot system	strand specific	paired-end	PO:0009006
SRR5020964	shoot system	strand specific	paired-end	PO:0009006
SRR5020965	shoot system	strand specific	paired-end	PO:0009006
SRR5020966	shoot system	strand specific	paired-end	PO:0009006
SRR5020967	shoot system	strand specific	paired-end	PO:0009006
SRR5020991	flower meristem	strand specific	paired-end	PO:0000229
SRR5020992	flower meristem	strand specific	paired-end	PO:0000229
SRR5020993	flower meristem	strand specific	paired-end	PO:0000229
SRR5020990	root	strand specific	paired-end	PO:0009005
SRR5020977	leaf	strand specific	paired-end	PO:0025034
SRR5020978	leaf	strand specific	paired-end	PO:0025034
SRR5020979	leaf	strand specific	paired-end	PO:0025034
SRR5020980	leaf	strand specific	paired-end	PO:0025034
SRR5020981	leaf	strand specific	paired-end	PO:0025034
SRR5020982	leaf	strand specific	paired-end	PO:0025034
SRR5020974	leaf	strand specific	paired-end	PO:0025034
SRR5020975	leaf	strand specific	paired-end	PO:0025034
SRR5020976	leaf	strand specific	paired-end	PO:0025034
SRR5020971	leaf	strand specific	paired-end	PO:0025034
SRR5020972	leaf	strand specific	paired-end	PO:0025034
SRR5020973	leaf	strand specific	paired-end	PO:0025034
SRR5020968	leaf	strand specific	paired-end	PO:0025034
SRR5020969	leaf	strand specific	paired-end	PO:0025034
SRR5020970	leaf	strand specific	paired-end	PO:0025034
SRR5020961	shoot system	strand specific	paired-end	PO:0009006
SRR5020962	shoot system	strand specific	paired-end	PO:0009006
SRR5020963	shoot system	strand specific	paired-end	PO:0009006
SRR3239441	shoot system	strand specific	paired-end	PO:0009006
SRR3239440	shoot system	strand specific	paired-end	PO:0009006
SRR3239432	shoot system	strand specific	paired-end	PO:0009006
SRR3239430	shoot system	strand specific	paired-end	PO:0009006
SRR3239420	shoot system	strand specific	paired-end	PO:0009006
SRR3239418	shoot system	strand specific	paired-end	PO:0009006
SRR3239416	shoot system	strand specific	paired-end	PO:0009006
SRR3239399	shoot system	strand specific	paired-end	PO:0009006
SRR3176679	crown root	strand specific	paired-end	PO:0000043
SRR3176675	crown root	strand specific	paired-end	PO:0000043
SRR3176671	crown root	strand specific	paired-end	PO:0000043
SRR3176667	crown root	strand specific	paired-end	PO:0000043
SRR3176663	crown root	strand specific	paired-end	PO:0000043
SRR3176657	crown root	strand specific	paired-end	PO:0000043
SRR3176678	stem	strand specific	paired-end	PO:0009047
SRR3176674	stem	strand specific	paired-end	PO:0009047
SRR3176670	stem	strand specific	paired-end	PO:0009047
SRR3176666	stem	strand specific	paired-end	PO:0009047
SRR3176662	stem	strand specific	paired-end	PO:0009047
SRR3176656	stem	strand specific	paired-end	PO:0009047
SRR3176677	crown root	strand specific	paired-end	PO:0000043
SRR3176673	crown root	strand specific	paired-end	PO:0000043
SRR3176669	crown root	strand specific	paired-end	PO:0000043
SRR3176665	crown root	strand specific	paired-end	PO:0000043
SRR3176661	crown root	strand specific	paired-end	PO:0000043
SRR3176659	crown root	strand specific	paired-end	PO:0000043
SRR3176676	stem	strand specific	paired-end	PO:0009047
SRR3176672	stem	strand specific	paired-end	PO:0009047
SRR3176668	stem	strand specific	paired-end	PO:0009047
SRR3176664	stem	strand specific	paired-end	PO:0009047
SRR3176660	stem	strand specific	paired-end	PO:0009047
SRR3176658	stem	strand specific	paired-end	PO:0009047
ERR385863	mesophyll	strand specific	paired-end	PO:0006070
ERR385862	mesophyll	strand specific	paired-end	PO:0006070
ERR385861	mesophyll	strand specific	paired-end	PO:0006070
ERR385866	mesophyll	strand specific	paired-end	PO:0006070
ERR385865	mesophyll	strand specific	paired-end	PO:0006070
ERR385864	mesophyll	strand specific	paired-end	PO:0006070
