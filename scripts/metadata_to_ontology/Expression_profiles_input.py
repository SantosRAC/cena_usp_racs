import argparse

# Add parser
version = 0.01
parser = argparse.ArgumentParser(description="Create the CoNekT expression profle input.",
                                 add_help=True)
parser.add_argument('-v', '--version', action='version', version=version)
parser.add_argument('-input_table', dest='input_tb', metavar='table.tsv',
                    type=str, help='Table.tsv', required=True)

args = parser.parse_args()
input_table = args.input_tb

# Open a output file to script
output = open("expression_profile_input.txt","w")
output.write("SampleID\tConditionDescription\tOntoID\n")

# Read the reviewed table
table = open(input_table,"r")
for row in table:
    lemma,terms,sra_str = row.strip("\n").split("\t")
    sra_list = sra_str.split(", ")
    if ";" in terms:
        #sra_dict = dict()
        terms_list = terms.split(";")
        onto_dict = dict()
        for item in terms_list:
            term,id = item.split(">")
            onto_dict[term] = id
        for sra in sra_list:
            output.write(f"{sra}\t{str(list(onto_dict.keys())).strip('[]')}\t{str(list(onto_dict.values())).strip('[]')}\n")
    else:
        term,id = terms.split(">")
        for sra in sra_list:
            output.write(f"{sra}\t{term}\t{id}\n")

# Close files
table.close()
output.close()