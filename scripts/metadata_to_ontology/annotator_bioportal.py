import urllib.request, urllib.error, urllib.parse
import json
import os
from pprint import pprint
import argparse
import time

version = 0.01
parser = argparse.ArgumentParser(description="Return the SRA's and the associated ontology terms.",
                                 add_help=True)
parser.add_argument('-v', '--version', action='version', version=version)
parser.add_argument('-input_table', dest='input_tb', metavar='table.tsv',
                    type=str, help='Table.tsv', required=True)

args = parser.parse_args()
input_table = args.input_tb

REST_URL = "http://data.bioontology.org"
API_KEY = "c818a604-1109-472c-b9dc-903523e18855"   

#Ontologies in this key: 
#   Plant Trait Ontology (PTO)
#   Plant Stress Ontology (PLANTSO)
#   Plant Experimental Conditions Ontology (PECO)
#   Plant Ontology (PO)


def get_json(url):
    opener = urllib.request.build_opener()
    opener.addheaders = [('Authorization', 'apikey token=' + API_KEY)]
    return json.loads(opener.open(url).read())

"""    for result in annotations:
        class_details = result["annotatedClass"]
        if get_class:
            try:
                class_details = get_json(result["annotatedClass"]["links"]["self"])
            except urllib.error.HTTPError:
                print(f"Error retrieving {result['annotatedClass']['@id']}")
                continue
        print("Class details")
        print("\tid: " + class_details["@id"])
        print("\tprefLabel: " + class_details["prefLabel"])
        print("\tontology: " + class_details["links"]["ontology"])"""

#Score to determine the number of ontologies obtained
#total_sentence = 0
total_sra_count = 0
total_sra_annotated = 0

count_line = 0

#file_lemmatized = open(f"{input_table.replace('_words.txt','_ontologie_terms.txt')}","w")

input_table_obj = open(input_table,"r")
for line in input_table_obj:
    #print(input_table)
    sentence,sra = line.strip("\n").split("\t")
    annotations = get_json(REST_URL + "/annotator?text=" + urllib.parse.quote(sentence))
    count_line += 1
    sra_list = sra.split(",")
    total_sra_count += len(sra_list)
    if annotations:
        total_sra_annotated += len(sra_list)
        for result in annotations:
            class_details = result["annotatedClass"]
            try:
                class_details = get_json(result["annotatedClass"]["links"]["self"])
            except urllib.error.HTTPError:
                print(f"Error retrieving {result['annotatedClass']['@id']}")
                continue
            #file_lemmatized.write(f'{sra}\t{sentence}\t{class_details["prefLabel"]}\n')
            print(f'#Line: {count_line}\n#Sentence: {sentence}\n#Term recovered: {class_details["prefLabel"]}\n')
#Link to OntoBee: {class_details["@id"]}\n#Defninition: {str("; ".join(class_details["definition"]))}\n#Number of datasets: {len(sra_list)}\n')
            #print(f""" {sentence}\t{str(sra_list)}\t{class_details["prefLabel"]}\t{class_details["@id"]}\t{str('; '.join(class_details["definition"]))}\n\
#""")

"""Função 'RECOMMENDER' do BioPortal"""

"""print(f"Plant Ontologie(PO)\nPorcentagem: {round(total_sra_annotated/total_sra_count*100, 2)}\n\
Número total de SRA: {total_sra_count}\n\
Total de SRA com anotação: {total_sra_annotated}")"""

#file_lemmatized.close()