import urllib.request, urllib.error, urllib.parse
import json
import os
import argparse
from pprint import pprint

version = 0.01
parser = argparse.ArgumentParser(description="Return the SRA's and the associated ontology terms.",
                                 add_help=True)
parser.add_argument('-v', '--version', action='version', version=version)
parser.add_argument('-input_table', dest='input_tb', metavar='table.tsv',
                    type=str, help='Table.tsv', required=True)

args = parser.parse_args()
input_table = args.input_tb

REST_URL = "http://data.bioontology.org"
API_KEY = "c818a604-1109-472c-b9dc-903523e18855"

def get_json(url):
    opener = urllib.request.build_opener()
    opener.addheaders = [('Authorization', 'apikey token=' + API_KEY)]
    return json.loads(opener.open(url).read())

# Open a new file with ontolgies recovered
onto_file = open(f"{input_table.replace('_lemma_out.txt','_biop_out.txt')}","w")

# Get list of search terms
input_table_obj = open(input_table,"r")
for line in input_table_obj:
    sentence,sra = line.strip("\n").split("\t")
    word_by_sentence = sentence.split(';')

    # Do a search for every term
    id2term = {}
    for word in word_by_sentence:
        onto_json = get_json(REST_URL + "/search?q=" + word.strip('\n') + "&ontologies=PAE%2CPO")["collection"]
        if len(onto_json) != 0:
            for item_json in onto_json[0:3]:
                #pprint(onto_json)
                id2term[item_json['@id'].strip('http://purl.obolibrary.org/obo/')] = item_json['prefLabel']
                #definition = onto_json[0]['definition'][0]
    onto_file.write(f"""{sentence}\t{";".join([f"{term}>{id.replace('_',':')}" for id,term in id2term.items()])}\t{sra}\n""")

onto_file.close()
