import argparse

# Add parser
version = 0.01
parser = argparse.ArgumentParser(description="Create the CoNekT expression profle input.",
                                 add_help=True)
parser.add_argument('-v', '--version', action='version', version=version)
parser.add_argument('-input_table', dest='input_tb', metavar='table.tsv',
                    type=str, help='Table.tsv', required=True)

args = parser.parse_args()
input_table = args.input_tb

# Open a output file to script
output = open(f"{input_table.replace('_biop_out.txt','_exp_pro_int.txt')}","w")
output.write("SampleID\tConditionDescription\tStrandness\tLayout\tPO_column\tPECO_column\n")

# Read the reviewed table
table = open(input_table,"r")
for row in table:
    lemma,terms,sra_str = row.strip("\n").split("\t")
    sra_list = sra_str.split(", ")
    if ";" in terms:
        terms_list = terms.split(";")
        list_onto = list()
        for item in terms_list:
            term,id = item.split(">")
            #onto = id + '(' + term + ')'
            list_onto.append(id)
        sort_list = sorted(list_onto, reverse=True)
        for sra in sra_list:
            output.write(f"{sra}\t{sort_list[0]}\t{sort_list[1]}\n")
    else:
        term,id = terms.split(">")
        for sra in sra_list:
            output.write(f"{sra}\t{term}\tstrand specific\tpaired-end\t{id}\n")

# Close files
table.close()
output.close()