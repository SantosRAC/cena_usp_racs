#!/usr/bin/env python
import argparse
import spacy
import sqlite3

version = 0.01
parser = argparse.ArgumentParser(description='Lemmatization of terms in our SRA metadata db.',
                                 add_help=True)
parser.add_argument('-v', '--version', action='version', version=version)
parser.add_argument('-database', dest='db', metavar='database.db',
                    type=str, help='SQLite3 database file.', required=True)

args = parser.parse_args()
database_name = args.db

# A trained pipeline in english able to analysis vocabulary, syntax and entities
nlp = spacy.load('en_core_web_sm')

# Function that depurate the not informative words
def lemmatize_sentence(sentence, sp):
    """Lemmatization of a sentence."""
    lemmalist = []
    exclude_terms = ['gene', 'expression', 'day', 'stage', 'sample',
                'profiling', 'seq', 'rna', 'analysis', 'filling', 'line',
                'plant', 'transcriptome', 'experiment', 'rnai', 'rep',
                'hour', 'week', 'control', 'rnaseq', 'fungi', 'transgenic',
                'della', 'llhc', 'deg', 'group', 'type', 'flag', 'fonio',
                'year','dna','rna','μmol','photon','','tissue','genetic','organism']
    sp_name_list = sp.split(' ')
    doc2 = nlp(sentence)
    for token in doc2:
        if (token.is_alpha) \
        and (token.text not in sp_name_list) \
        and (not token.is_stop) \
        and (token.pos_ in ["NOUN","PROPN","ADJ"]) \
        and (2 < len(token.lemma_.lower()) \
        and (token.lemma_.lower() not in exclude_terms)):
            if token.lemma_.lower() not in lemmalist:
                lemmalist.append(token.lemma_.lower())
    
    return lemmalist

# Query database and store sentences to be lemmatisized
# Connect to sqlite3 database
conn2sra_metadata_db = sqlite3.connect(database_name)
c = conn2sra_metadata_db.cursor()

c.execute("SELECT DISTINCT species_name FROM sra_metadata")
species_query = c.fetchall()

# Store the repeated PubMed ID and the SRR for same metadata description
srr_dict = {}
pmid_dict = {}
instance2sra = {}

# Select and handle the data in SQLite collumns
for i, line in enumerate(species_query):
    species_field = species_query[i][0]
    pmid_dict[species_field]={}
    srr_dict[species_field]={} 
    c.execute("""SELECT DISTINCT sra_id,
                    strand_info,
                    ncbi_expid,
                    pmid,
                    ncbi_bioproject_id,
                    ncbi_biosample_name,
                    treatment,
                    tissue,
                    dev_stage,
                    age,
                    source_name
                 FROM sra_metadata WHERE species_name=?""", (species_field,)
            )
    fetchall_1 = c.fetchall()
    for j, line2 in enumerate(fetchall_1):
        sra_id = fetchall_1[j][0]
        pmid_id = fetchall_1[j][3]
        attributes = " ".join(fetchall_1[j][5:])                    # Line with the main attributes to lemmatization
        attributes = attributes.replace('_',' ')
        lemmatize = lemmatize_sentence(attributes, species_field)   # Function will lemmatize the main attributes
        lemmatized = ";".join(lemmatize)       
        instance = str(lemmatized)
        
        if pmid_id:
            if instance not in instance2sra.keys():                     # Associate each instance lemmatized with his respective SRA
                instance2sra[instance]=[sra_id]
            else:
                instance2sra[instance].append(sra_id)

conn2sra_metadata_db.close()

# Creates a new file with the lemmatized words
word_lemmatized = open(f"{database_name.replace('.db','_lemma_out.txt')}","w")
for key,value in instance2sra.items():
    word_lemmatized.write(str(key)+"\t"+str(', '.join(value))+"\n")
word_lemmatized.close()