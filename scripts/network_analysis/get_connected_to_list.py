#!/usr/bin/env python3

import argparse
import networkx as nx

version = 0.01
parser = argparse.ArgumentParser(description='Recovers connections between selected genes and TFs in a network', add_help=True)
parser.add_argument('-v', '--version', action='version', version=version)
parser.add_argument('--verbose', action='store_true', help='Verbose output')
parser.add_argument('--network', dest='network_file', metavar='network.txt',
                    type=str, help='File network generated with pcc.py (LSTrAP)',
                    required=True)
parser.add_argument('--pcc_cutoff', dest='pcc_cutoff', metavar='0.7', default=0.7,
                    type=float, help='PCC cutoff to consider in network analysis',
                    required=False)
parser.add_argument('--limit', dest='limit', metavar='40', default=40,
                    type=int, help='Limit of links to consider in network analysis',
                    required=False)
parser.add_argument('--tf_list', dest='tf_list_file', metavar='tf_list.txt',
                    type=str, help='TF gene list (one gene per line)',
                    required=True)
parser.add_argument('--gene_list', dest='gene_list_file', metavar='gene_list.txt',
                    type=str, help='Target gene list (one gene per line)',
                    required=True)
parser.add_argument('--out', dest='out_prefix', metavar='out', default='out',
                    type=str, help='Output prefix for network analysis',
                    required=False)

args = parser.parse_args()

network_file = args.network_file
pcc_cutoff = args.pcc_cutoff
limit = args.limit
tf_list_file = args.tf_list_file
gene_list_file = args.gene_list_file
out_prefix = args.out_prefix

network = nx.Graph()

with open(network_file) as fin:
    print(f'Reading network...')
    for linenr, line in enumerate(fin):
        try:
            query, hits = line.strip().split(' ')
            query = query.replace(':', '')
        except ValueError:
            print("Error parsing line %d: \"%s\"" % (linenr, line))
            continue

        for i, h in enumerate(hits.split('\t')):
            try:
                name, value = h.split('(')
                value = float(value.replace(')', ''))
                if value >= pcc_cutoff:
                    if i <= limit:
                        network.add_edges_from([(query,
                                                name,
                                                {'pcc': value, 'score': i})
                                                ])
            except ValueError as e:
                print(f'Skipping {i}, {str(h)}\n')

tf_list = []

with open(tf_list_file) as nf:
    for line in nf:
        tf_gene, tf_annotation = line.rstrip('\n').split('\t')
        if tf_gene not in tf_list:
            tf_list.append(tf_gene)
            if tf_gene in network.nodes():
                network.nodes[tf_gene]['annotation'] = tf_annotation
            else:
                network.add_node(tf_gene, annotation=tf_annotation)

out_file = f'{out_prefix}_selected_pairs.txt'
pairs_out_file = open(out_file, 'w')

with open(gene_list_file) as nf:
    for line in nf:
        gene = line.rstrip('\n')
        if gene not in network.nodes():
            continue

        for edge in nx.edges(network, nbunch=gene):
            if network[edge[0]][edge[1]]['pcc'] > pcc_cutoff:
                if (edge[0] in tf_list) or (edge[1] in tf_list):
                    pairs_out_file.write(gene+'\t'+edge[1]+' ('+network.nodes[edge[1]]['annotation']+')\t'+str(network[edge[0]][edge[1]]['pcc'])+'\n')

pairs_out_file.close()