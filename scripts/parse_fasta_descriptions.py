#!/home/santosrac/anaconda3/bin/python3
"""Script that parses FASTA with name and description in SeqIO description."""

import argparse
from concurrent.futures.process import _threads_wakeups
from Bio import SeqIO

version=0.01
parser = argparse.ArgumentParser(description='Reads a FASTA with seq name and description returns a table with description', add_help=True)
parser.add_argument('-v','--version', action='version', version=version)
parser.add_argument('-o','--out', dest='out', metavar='table.txt', type=str, help='Output table with seq name and description', required=True)
parser.add_argument('-i','--in', dest='inFASTA', metavar='seq.fasta', type=str, help='FASTA with seq and description', required=True)

args = parser.parse_args()

fastaOBJ = args.inFASTA
tableOBJ = args.out
tableOUT = open(tableOBJ, "w")

for seq_record in SeqIO.parse(fastaOBJ, "fasta"):
    seq_description = seq_record.description
    seq_description = seq_description.replace(' ','\t', 1)
    tableOUT.write(seq_description+"\n")

tableOUT.close()
