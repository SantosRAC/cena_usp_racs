#!/usr/bin/env python

import argparse
from Bio import Entrez
import time
import sqlite3
import os

version = 0.01
parser = argparse.ArgumentParser(description='Insert strandness information into db.',
                                 add_help=True)
parser.add_argument('-v', '--version', action='version', version=version)
parser.add_argument('-e', '--email', dest='e_mail',
                    metavar='A.N.Other@example.com', type=str,
                    help='User e-mail', required=True)
parser.add_argument('--salmon_tbl', dest='salmon_tbl', metavar='salmon_summary.txt',
                    type=str, help='SRA identifier.', required=True)
parser.add_argument('-out', dest='output', metavar='out.txt',
                    type=str, help='Output tabular file.', required=False)
parser.add_argument('--database', dest='db', metavar='database.db',
                    type=str, help='SQLite3 database file.', required=True)

args = parser.parse_args()
email_address = args.e_mail
salmon_in_tbl = args.salmon_tbl
database_name = args.db

if not os.path.isfile(database_name):
    print(f'Database {database_name} does not exist.')
    exit(1)

#Connet to sqlite3 database
conn2sra_metadata_db = sqlite3.connect(database_name)

c = conn2sra_metadata_db.cursor()

Entrez.email = email_address

if args.output:
    outfile_obj = open(args.output, "a")
    outfile_obj.write("species\texp_id\tsra_id\tstrand_info\n")

with open(salmon_in_tbl, 'r') as reader:
    for line in reader:
        if line.startswith('Species'):
            continue
        if len(line.rstrip().split('\t')) == 3:
            species, sra_id, strand_info = line.rstrip().split('\t')
        else:
            print(f'***Skipping line:\n{line}\n')
            continue
        time.sleep(2)
        handle = Entrez.esearch(dbfrom="sra", term=sra_id+'[ACCN]', db='sra')
        record_sra = Entrez.read(handle)
        handle.close()
        exp_id = record_sra['IdList'][0]
        c.execute("""UPDATE sra_metadata SET strand_info = ? WHERE ncbi_expid = ?""", (strand_info, exp_id))
        conn2sra_metadata_db.commit()
        #print(type(sra_id), type(exp_id))
        print(f'Updated strandness ({strand_info}) info for {sra_id} ({species})')
        c.execute("""UPDATE sra_metadata SET sra_id = ? WHERE ncbi_expid = ?""", (sra_id, exp_id))
        conn2sra_metadata_db.commit()
        if args.output:
            outfile_obj.write(species+"\t"+exp_id+"\t"+sra_id+"\t"+strand_info+"\n")

if args.output:
    outfile_obj.close()
conn2sra_metadata_db.close()
