#!/usr/bin/env python

import json
import argparse
import subprocess

version = 0.01

parser = argparse.ArgumentParser(description='Recover URL for SRA and download reads.', add_help=True)
parser.add_argument('-v', '--version', action='version', version=version)
parser.add_argument('--verbose', dest='verbose', action='store_true')
parser.add_argument('-i', '--input_sra', dest='SRA_ids', metavar='SRA identifier(s)', help='Provide a list of SRA identifier(s)', required=True, nargs='+', default=[])
parser.add_argument('--log', dest='logfile', metavar='log file', type=str, help='Provide a name for log file)', required=True)

args = parser.parse_args()
sra_id_list = args.SRA_ids
logfile = args.logfile

logfile_obj = open(logfile, 'a')

for sra_id in sra_id_list:

    jsonf = sra_id+".json"

    bashCommand = "ffq --ftp "+sra_id+" -o "+jsonf
    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()

    jsonffq = open(jsonf, "r")
    jsonffq_obj = json.load(jsonffq)

    for sra_file in jsonffq_obj:
        print('Downloading fastq for:', jsonf)
        md5sra = sra_file['md5']
        bashCommand = "wget "+sra_file['url']
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()
        fastqfilelist = sra_file['url'].split("/")
        fastqfile = fastqfilelist[-1]
        bashCommand = "md5sum "+fastqfile
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        output, error = process.communicate()
        if md5sra == str(output).split(" ")[0].split("'")[1]:
            logfile_obj.write("SUCCESS:\t"+fastqfile+"\n")
        else:
            logfile_obj.write("FAILED:\t"+fastqfile+"\t"+sra_file['url']+"\n")
    jsonffq.close()

    bashCommand = "rm "+jsonf
    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()

logfile_obj.close()
