#!/usr/bin/env python

import argparse
import pandas as pd
import sqlite3

version = 0.01
parser = argparse.ArgumentParser(description='Import SRA information from file\
                    into database.', add_help=True)
parser.add_argument('-v', '--version', action='version', version=version)
parser.add_argument('--infile', dest='inputfile', metavar='metadata.txt',
                    type=str, help='File with SRA information.', required=True)
parser.parser.add_argument('--database', dest='db', metavar='database.db',
                    type=str, help='SQLite3 database file.', required=True)

args = parser.parse_args()
input_file = args.inputfile
database_name = args.db

#Connet to sqlite3 database
conn2sra_metadata_db = sqlite3.connect(database_name)

c = conn2sra_metadata_db.cursor()

# CREATE TABLE sra_metadata (if necessary)
c.execute("""CREATE TABLE IF NOT EXISTS sra_metadata (
        sra_id TEXT UNIQUE,
        strand_info TEXT,
        ncbi_expid INTEGER,
        ncbi_biosample_id TEXT,
        ncbi_biosample_name TEXT,
        ncbi_bioproject_id TEXT,
        species_name TEXT,
        species_cultivar TEXT,
        species_genotype TEXT,
        treatment TEXT,
        dev_stage TEXT,
        tissue TEXT,
        age TEXT,
        source_name TEXT
    )""")

conn2sra_metadata_db.commit()

with pd.read_table(input_file, encoding='utf-8', header=None, chunksize=1) as reader:
    for chunk in reader:
        exp_id, samn_id, samn_name, prj_id, input_species, cultivar, genotype, treatment, dev_stage, tissue, age, source_name = chunk.values.flatten().tolist()
        if str(exp_id).startswith('SRA_id'):
            continue
        else:
            print(exp_id, samn_id, samn_name, prj_id, input_species, cultivar, genotype, treatment, dev_stage, tissue, age, source_name)
            c.execute("""INSERT INTO sra_metadata (ncbi_expid,\
                        ncbi_biosample_id, ncbi_biosample_name,\
                        ncbi_bioproject_id, species_name, species_cultivar,\
                        species_genotype, treatment, dev_stage, tissue, age,\
                        source_name) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""",
                        (exp_id, samn_id, samn_name, prj_id, input_species,
                        cultivar, genotype, treatment, dev_stage, tissue, age,
                        source_name)
                      )
            conn2sra_metadata_db.commit()

conn2sra_metadata_db.close()