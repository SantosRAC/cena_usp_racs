#!/home/santosrac/anaconda3/bin/python3

import argparse
from Bio import Entrez
import time

version = 0.01
parser = argparse.ArgumentParser(description='Returns table from get_metadata \
                                 with SRA id information.',
                                 add_help=True)
parser.add_argument('-v', '--version', action='version', version=version)
parser.add_argument('-file1', dest='file1', metavar='in1.txt',
                    type=str, help='File with two columns: ncbi id and sra id (stranded)',
                    required=True)
parser.add_argument('-file2', dest='file2', metavar='in2.txt',
                    type=str, help='File with two columns: ncbi id and sra id (unstranded)',
                    required=True)
parser.add_argument('-file3', dest='file3', metavar='in3.txt',
                    type=str, help='File from get_metadata.', required=True)
parser.add_argument('-out', dest='output', metavar='out.txt',
                    type=str, help='Output tabular file.', required=True)

args = parser.parse_args()
infile1 = args.file1
infile2 = args.file2
infile3 = args.file3
outfile = args.output

outfile_obj = open(outfile, "a")

ncbi_id_sra_stranded = {}
ncbi_id_sra_unstranded = {}
ncbi_id_sra = {}

with open(infile1, 'r') as infile1obj:
    for line in infile1obj:
        line = line.strip()
        ncbi_id, sra_id = line.split("\t")
        if ncbi_id in ncbi_id_sra_stranded.keys():
            ncbi_id_sra_stranded[ncbi_id].append(sra_id)
        else:
            ncbi_id_sra_stranded[ncbi_id] = [sra_id]

with open(infile2, 'r') as infile2obj:
    for line in infile2obj:
        line = line.strip()
        ncbi_id, sra_id = line.split("\t")
        if ncbi_id in ncbi_id_sra_unstranded.keys():
            ncbi_id_sra_unstranded[ncbi_id].append(sra_id)
        else:
            ncbi_id_sra_unstranded[ncbi_id] = [sra_id]

outfile_obj.write("ncbi_id\tsra_id\tbiosample_id\tbiosample_name\t\
bioproject_id\tspecies\tcultivar\tgenotype\ttreatment\
\tdev_stage\ttissue\tage\tsource_name\n")

with open(infile3, 'r') as infile3obj:
    for line in infile3obj:
        ncbi_id,biosample_id,biosample_name,bioproject_id,species,cultivar,genotype,treatment,dev_stage,tissue,age,source_name = line.split("\t")
        source_name = source_name.strip()
        if ncbi_id not in ncbi_id_sra_stranded.keys():
            if ncbi_id in ncbi_id_sra_unstranded.keys():
                print(ncbi_id, "unstranded (checked with salmon)")
            else:
                print(ncbi_id, "weird case")
        else:
            for sra_id in ncbi_id_sra_stranded[ncbi_id]:
                outfile_obj.write(ncbi_id+"\t"+sra_id+"\t"+biosample_id+"\t"+biosample_name \
                      +"\t"+bioproject_id+"\t"+species+"\t"+cultivar \
                      +"\t"+genotype+"\t"+treatment+"\t"+dev_stage+"\t"+tissue \
                      +"\t"+age+"\t"+source_name+"\n")

outfile_obj.close()
