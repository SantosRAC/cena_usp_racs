#!/usr/bin/env python

from Bio import SeqIO
import argparse

parser = argparse.ArgumentParser(description='Fix fasta file')
parser.add_argument('-i', '--input', dest='infasta', metavar='sequences.fasta',
                    type=str, help='Input fasta file', required=True)
parser.add_argument('-o', '--output', dest='outfasta', metavar='sequences_out.fasta',
                    type=str, help='Output fasta file', required=True)

args = parser.parse_args()
in_fasta = args.infasta
out_fasta = args.outfasta

# Iterate over the sequences in the input fasta file
with open(in_fasta, 'r'):
    for record in SeqIO.parse(in_fasta, 'fasta'):
        outfasta_obj = open(out_fasta, 'a')
        outfasta_obj.write('>' + record.id + '\n' + str(record.seq) + '\n')
        outfasta_obj.close()

